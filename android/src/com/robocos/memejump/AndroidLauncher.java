package com.robocos.memejump;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.security.NetworkSecurityPolicy;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.WindowManager;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Permission;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesActivityResultCodes;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.achievement.Achievement;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.Invitations;
import com.google.android.gms.games.multiplayer.Multiplayer;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;
import com.google.example.games.basegameutils.BaseGameActivity;
import com.google.example.games.basegameutils.GameHelper;
import com.robocos.memejump.entities.Meme;
import com.robocos.memejump.entities.PlayerMP;
import com.robocos.memejump.entities.Tile;
import com.robocos.memejump.googleplay.ActionResolver;
import com.robocos.memejump.googleplay.packets.Packet;
import com.robocos.memejump.googleplay.packets.PacketPosition;
import com.robocos.memejump.googleplay.packets.PacketStatus;
import com.robocos.memejump.googleplay.packets.PacketUserInfo;
import com.robocos.memejump.googleplay.packets.PacketWorldData;
import com.robocos.memejump.screens.GameScreen;
import com.robocos.memejump.screens.MultiplayerScreen;

import android.Manifest;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AndroidLauncher extends AndroidApplication implements GameHelper.GameHelperListener, ActionResolver {

    private GPNetworker gameHelper;
    private InterstitialAd ad;
    private final int REQUEST_CODE_NETWORK = 2;
    private MemeJump game;
    private int WIDTH, HEIGHT;

    private boolean paused;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.game = new MemeJump(this);
        gameHelper = new GPNetworker(this, GameHelper.CLIENT_GAMES);


        String[] permissions = {Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.GET_ACCOUNTS, Manifest.permission.INTERNET};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !permissionsGranted(permissions)) {
            requestPermissions(permissions, REQUEST_CODE_NETWORK);
        } else {
            gameHelper.enableDebugLog(true);
            gameHelper.setup(this);
            gameHelper.setGame(game);
            gameHelper.setApiClient(gameHelper.getApiClient());
        }


        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useGLSurfaceView20API18 = true;
        initialize(game, config);
        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        Gdx.app.log("MEME_JUMP", "Starting up Meme Jump");
        Gdx.app.log("MEME_JUMP", "Written by robocos987/Waleed Ghazal");
        Gdx.app.log("MEME_JUMP", "Initializing game..");

        ad = new InterstitialAd(this);
        ad.setAdUnitId("ca-app-pub-5703149111787785/3447822558");

        ad.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                paused = false;
            }

            @Override
            public void onAdOpened() {
                paused = true;
            }
        });

        checkInvites();

        WIDTH = game.WIDTH;
        HEIGHT = game.HEIGHT;


    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_NETWORK) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permissions granted!", Toast.LENGTH_SHORT).show();
            } else {
                // showRationale = false if user clicks Never Ask Again, otherwise true
                boolean showRationale = false;
                for (String s : permissions)
                    showRationale = shouldShowRequestPermissionRationale(s);
                if (showRationale) {
                    gameHelper.enableDebugLog(true);
                    gameHelper.setup(this);
                    gameHelper.setGame(game);
                    gameHelper.setApiClient(gameHelper.getApiClient());
                } else {
                    Toast.makeText(this, "Read Contacts permission denied", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean permissionsGranted(String[] perms) {
        boolean granted = false;
        for (String perm : perms)
            granted = ContextCompat.checkSelfPermission(this, perm) == PackageManager.PERMISSION_GRANTED;
        return granted;
    }

    @Override
    public boolean achievementUnlocked(String achievement) {
        boolean fullLoad = false;  // set to 'true' to reload all achievements (ignoring cache)
        long waitTime = 4;    // seconds to wait for achievements to load before timing out
        boolean unlocked = false;

        // load achievements
        PendingResult p = Games.Achievements.load(gameHelper.getApiClient(), fullLoad);
        Achievements.LoadAchievementsResult r = (Achievements.LoadAchievementsResult) p.await(waitTime, TimeUnit.SECONDS);
        int status = r.getStatus().getStatusCode();
        if (status != GamesStatusCodes.STATUS_OK) {
            r.release();
            return true;           // Error Occurred
        }

        // process the loaded achievements
        AchievementBuffer buf = r.getAchievements();
        int bufSize = buf.getCount();
        for (int i = 0; i < bufSize; i++) {
            Achievement ach = buf.get(i);

            // here you now have access to the achievement's data
            if (achievement.equals(ach.getAchievementId()))  // the achievement ID string
                unlocked = ach.getState() == Achievement.STATE_UNLOCKED;  // is unlocked
        }
        buf.close();
        r.release();
        return unlocked;
    }

    @Override
    public void showAd() {
        try {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (ad.isLoaded()) {
                        ad.show();
                    } else {
                        AdRequest interstitialRequest = new AdRequest.Builder().build();
                        ad.loadAd(interstitialRequest);
                    }
                }
            });
        } catch (Exception e) {
        }

        Gdx.app.log("MEME_JUMP", "Displaying ad..");
    }

    @Override
    public void quickGame() {
        gameHelper.quickGame();
    }


    @Override
    public void onStart() {
        super.onStart();
        gameHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        gameHelper.onStop();
    }

    @Override
    public void onActivityResult(int request, int response, Intent data) {
        super.onActivityResult(request, response, data);
        gameHelper.onActivityResult(request, response, data);
    }

    @Override
    public boolean getSignedInGPGS() {
        return gameHelper.isSignedIn();
    }

    @Override
    public boolean isConnected() {
        return gameHelper.getApiClient().isConnected();
    }

    @Override
    public void loginGPGS() {
        try {
            runOnUiThread(new Runnable() {
                public void run() {
                    gameHelper.beginUserInitiatedSignIn();
                }
            });
        } catch (final Exception ex) {
        }
    }


    @Override
    public void submitScoreGPGS(int score) {
        Games.Leaderboards.submitScore(gameHelper.getApiClient(), "CgkI4-GW2_YVEAIQBg", score);
    }

    @Override
    public void unlockAchievementGPGS(String achievementId) {
        Games.Achievements.unlock(gameHelper.getApiClient(), achievementId);
    }

    @Override
    public void getLeaderboardGPGS() {
        if (gameHelper.isSignedIn()) {
            startActivityForResult(Games.Leaderboards.getLeaderboardIntent(gameHelper.getApiClient(), "CgkI4-GW2_YVEAIQBg"), 100);
        } else if (!gameHelper.isConnecting()) {
            loginGPGS();
        }
    }

    @Override
    public void getAchievementsGPGS() {
        if (gameHelper.isSignedIn()) {
            startActivityForResult(Games.Achievements.getAchievementsIntent(gameHelper.getApiClient()), 101);
        } else if (!gameHelper.isConnecting()) {
            loginGPGS();
        }
    }

    @Override
    public void onSignInFailed() {
    }

    @Override
    public void onSignInSucceeded() {
    }

    @Override
    public boolean isPaused() {
        return paused;
    }

    @Override
    public void sendPos(Vector2 pos, boolean flip) {
        gameHelper.sendPos(pos, flip);
    }


    @Override
    public void sendStatus(int SCORE, int MEMES, boolean dead) {
        gameHelper.sendStatus(SCORE, MEMES, dead);
    }

    @Override
    public void initMatch() {
        gameHelper.initMatch();

    }

    @Override
    public String getUsername() {
        return Games.Players.getCurrentPlayer(gameHelper.getApiClient()).getDisplayName();
    }

    @Override
    public void sendStatus(int score, int memes, boolean dead, int id) {
        gameHelper.sendStatus(score, memes, dead, id);
    }

    @Override
    public void leaveGame() {
        gameHelper.reset();
    }

    public void checkInvites() {
        if (!gameHelper.isSignedIn()) return;
        System.out.println("Checking invites");
        PendingResult<Invitations.LoadInvitationsResult> invs = Games.Invitations.loadInvitations(gameHelper.getApiClient());
        invs.setResultCallback(new ResultCallback<Invitations.LoadInvitationsResult>() {
            @Override
            public void onResult(Invitations.LoadInvitationsResult list) {
                if (list == null) return;
                if (list.getInvitations().getCount() > 0) {
                    gameHelper.invitationInbox();
                }
            }
        });
    }


    /**
     * Created by walee on 9/23/2016.
     */

    class GPNetworker extends GameHelper implements RoomUpdateListener, RealTimeMessageReceivedListener, RoomStatusUpdateListener, OnInvitationReceivedListener {
        /**
         * Construct a GameHelper object, initially tied to the given Activity.
         * After constructing this object, call @link{setup} from the onCreate()
         * method of your Activity.
         *
         * @param activity
         * @param clientsToUse the API clients to use (a combination of the CLIENT_* flags,
         */

        static final int RC_SELECT_PLAYERS = 454545, RC_WAITING_ROOM = 423523, RC_INVITATION_INBOX = 474757, REQUEST_RESOLVE_ERROR = 569030;
        private final int MIN_PLAYERS = 1, MAX_PLAYERS = 1;
        private Activity activity;
        private String mRoomID;
        private MemeJump game;
        private MultiplayerScreen world;

        private ArrayList<Participant> participants;

        private final int PACKET_PING = 0, PACKET_WORLD_DATA = 1, PACKET_POSITION = 2, PACKET_USERINFO = 3, PACKET_STATUS = 4;
        public String myID;

        private GoogleApiClient client;
        private boolean left, mResolvingError;


        @Override
        public void onConnectionFailed(ConnectionResult result) {
            if (mResolvingError) {
                return;
            } else if (result.hasResolution()) {
                try {
                    mResolvingError = true;
                    result.startResolutionForResult(activity, REQUEST_RESOLVE_ERROR);
                } catch (IntentSender.SendIntentException e) {
                    // There was an error with the resolution intent. Try again.
                    if (client != null)
                        client.connect();
                }
            } else {
                mResolvingError = true;
            }
        }

        public GPNetworker(Activity activity, int clientsToUse) {
            super(activity, clientsToUse);
            this.activity = activity;
            participants = new ArrayList<Participant>();

        }

        public void setApiClient(GoogleApiClient client) {
            this.client = client;
            client.registerConnectionCallbacks(this);
        }

        public void setGame(MemeJump game) {
            this.game = game;
        }

        public void quickGame() {
            Bundle am = RoomConfig.createAutoMatchCriteria(MIN_PLAYERS, MAX_PLAYERS, 0);
            RoomConfig.Builder roomConfigBuilder = makeBasicRoomConfigBuilder();
            roomConfigBuilder.setAutoMatchCriteria(am);
            RoomConfig roomConfig = roomConfigBuilder.build();
            Games.RealTimeMultiplayer.create(client, roomConfig);

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }
            });

        }

        public void initMatch() {
            Intent intent = Games.RealTimeMultiplayer.getSelectOpponentsIntent(client, MIN_PLAYERS, MAX_PLAYERS);
            this.activity.startActivityForResult(intent, RC_SELECT_PLAYERS);
        }

        private RoomConfig.Builder makeBasicRoomConfigBuilder() {
            return RoomConfig.builder(this)
                    .setMessageReceivedListener(this)
                    .setRoomStatusUpdateListener(this);
        }


        @Override
        public void onActivityResult(int request, int response, Intent data) {
            if (request == GPNetworker.RC_WAITING_ROOM) {
                if (response == Activity.RESULT_CANCELED || response == GamesActivityResultCodes.RESULT_LEFT_ROOM) {
                    if (client.isConnected()) {
                        Games.RealTimeMultiplayer.leave(client, this, mRoomID);
                        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                        game.displayError("Player has cancelled!");
                    }
                } else {
                    client.connect();
                }
            } else if (request == GPNetworker.RC_SELECT_PLAYERS) {
                if (response != Activity.RESULT_OK) {
                    game.displayError("Player declined");
                    return;
                }
            }else if (request == RC_INVITATION_INBOX) {
                if (response != Activity.RESULT_OK)
                    return;

                // get the selected invitation
                Bundle extras = data.getExtras();
                Invitation invitation =
                        extras.getParcelable(Multiplayer.EXTRA_INVITATION);

                // accept it!
                RoomConfig roomConfig = makeBasicRoomConfigBuilder()
                        .setInvitationIdToAccept(invitation.getInvitationId())
                        .build();
                Games.RealTimeMultiplayer.join(client, roomConfig);

                // prevent screen from sleeping during handshake
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

                // go to game screen
                createWorld();
                game.setScreen(world);
            }

            // get the invitee list
       /*     Bundle extras = data.getExtras();
            final ArrayList<String> invitees =
                    data.getStringArrayListExtra(Games.EXTRA_PLAYER_IDS);

            // get auto-match criteria
            Bundle autoMatchCriteria = null;
            int minAutoMatchPlayers =
                    data.getIntExtra(Multiplayer.EXTRA_MIN_AUTOMATCH_PLAYERS, 0);
            int maxAutoMatchPlayers =
                    data.getIntExtra(Multiplayer.EXTRA_MAX_AUTOMATCH_PLAYERS, 0);
            Gdx.app.log("Player", "Pmin" + minAutoMatchPlayers + " Pmax:" + maxAutoMatchPlayers);
            for (String invitee : invitees) {
                Gdx.app.log("L", invitee);
            }
            if (minAutoMatchPlayers > 0) {
                autoMatchCriteria = RoomConfig.createAutoMatchCriteria(
                        minAutoMatchPlayers, maxAutoMatchPlayers, 0);
            } else {
                autoMatchCriteria = null;
            }

            // create the room and specify a variant if appropriate
            RoomConfig.Builder roomConfigBuilder = makeBasicRoomConfigBuilder();
            roomConfigBuilder.addPlayersToInvite(invitees);
            if (autoMatchCriteria != null) {
                roomConfigBuilder.setAutoMatchCriteria(autoMatchCriteria);
            }
            RoomConfig roomConfig = roomConfigBuilder.build();
            Games.RealTimeMultiplayer.create(client, roomConfig);

            // prevent screen from sleeping during handshake

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }
            });*/
        }


        @Override
        public void onRealTimeMessageReceived(RealTimeMessage realTimeMessage) {
            byte[] message = realTimeMessage.getMessageData();
            String sender = realTimeMessage.getSenderParticipantId();

            Object msg = deserialize(message);
            if (msg instanceof Packet) {
                Packet packet = (Packet) msg;
                int packetID = packet.packetID;

                Gdx.app.log("MEME_JUMP", "Received packet with ID: " + packetID);
                int WIDTH = game.WIDTH;
                int HEIGHT = game.HEIGHT;

                switch (packetID) {
                    case PACKET_PING:
                        System.out.println("Ping");
                        Packet packetPing = new Packet();
                        packetPing.packetID = PACKET_PING;
//                    byte[] testPacket = serialize(packetPing);
//                    for(Participant participant: participants)
//                        if(!participant.getParticipantId().equals(myID))
//                            Games.RealTimeMultiplayer.sendReliableMessage(client, null, testPacket, mRoomID, participant.getParticipantId());
                        break;
                    case PACKET_WORLD_DATA:
                        PacketWorldData packetWorldData = (PacketWorldData) packet;

                        float PLAYER_HEIGHT = (WIDTH / 20f) * 2f;

                        Tile[] tiles = new Tile[packetWorldData.tiles.length];
                        for (int i = 0; i < tiles.length; i++) {
                            tiles[i] = new Tile(packetWorldData.tiles[i].id, (i + 0.5f) * (HEIGHT / 3.65f) + 1.5f * PLAYER_HEIGHT, (WIDTH / 2.5f), (WIDTH / 20f) * 3.5f,
                                    packetWorldData.tiles[i].right);
                        }
                        world.setTileSet(tiles);

                        Meme[] memes = new Meme[packetWorldData.memes.length];
                        for (int i = memes.length - 1; i >= 0; i--)
                            memes[i] = new Meme(i + 1, ((WIDTH - 400) / 2f - WIDTH / 16f) + packetWorldData.memes[i].x,
                                    (i + 0.2f) * packetWorldData.memes[i].y + 3 * PLAYER_HEIGHT, 100f, 100f,
                                    packetWorldData.memes[i].skin);
                        world.setMemes(memes);

                        sendUserInfo(world.player.getSkin(), world.player.getUsername());
                        sendPos(new Vector2(world.player.pos.x / WIDTH, world.player.pos.y / HEIGHT), false);
                        break;
                    case PACKET_USERINFO:
                        PacketUserInfo packetUserInfo = (PacketUserInfo) packet;
                        world.addPlayer(packetUserInfo.username, sender, packetUserInfo.skin);
                        break;
                    case PACKET_POSITION:
                        PacketPosition packetPos = (PacketPosition) packet;
//                    if(world.getPlayerFromID(sender) != null)
                        if (world.getPlayerFromID(sender) != null) {
                            PlayerMP playerMP = world.getPlayerFromID(sender);
                            playerMP.netPos = new Vector2(packetPos.pos.x * WIDTH, packetPos.pos.y * HEIGHT);
                            playerMP.left = packetPos.flip;
                        }
                        break;
                    case PACKET_STATUS:
                        PacketStatus packetStatus = (PacketStatus) packet;
                        world.getPlayerFromID(sender).setStats(packetStatus.SCORE, packetStatus.MEMES, packetStatus.dead);
                        if (packetStatus.usedMeme > 0)
                            world.memes[packetStatus.usedMeme - 1].used = true;
                        System.out.println("Received status");
                        break;

                }
            }

        }

        @Override
        public void onRoomConnecting(Room room) {

        }

        @Override
        public void onRoomAutoMatching(Room room) {

        }

        @Override
        public void onPeerInvitedToRoom(Room room, List<String> list) {
            determineHost(room);
        }

        @Override
        public void onPeerDeclined(Room room, List<String> list) {
            game.displayError("Player declined");
        }

        @Override
        public void onPeerJoined(Room room, List<String> list) {
            System.out.println("A peer has joined");
            participants = room.getParticipants();
        }

        @Override
        public void onPeerLeft(Room room, List<String> list) {
            game.displayError("Players have left");
            Gdx.app.log("MEME_JUMP", "Peer left");
        }

        @Override
        public void onConnectedToRoom(Room room) {
            determineHost(room);
        }

        private void createWorld() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    world = new MultiplayerScreen(game.screen, game.skin, game.username);
                }
            });
        }

        private void determineHost(Room room) {
            createWorld();
            game.setScreen(world);
            participants = room.getParticipants();

            myID = room.getParticipantId(Games.Players.getCurrentPlayer(client).getPlayerId());

            List<String> ids = sortParticipants(participants, myID);

            //first participant in the list is responsible for generating tiles
            System.out.println(ids.contains(myID));
            if (ids.get(0).equals(myID)) {
                float WIDTH = game.WIDTH;
                float HEIGHT = game.HEIGHT;

                float PLAYER_HEIGHT = (WIDTH / 20f) * 2f;
                System.out.println("Sending tiles");
                Tile[] tiles = generateTiles();
                Meme[] memes = generateMemes(tiles.length);
                PacketWorldData packetWorldData = new PacketWorldData();
                packetWorldData.tiles = tiles;
                packetWorldData.memes = memes;

                packetWorldData.packetID = PACKET_WORLD_DATA;

                Meme[] gameMemes = new Meme[memes.length];
                for (int i = 0; i < gameMemes.length; i++) {
                    gameMemes[i] = new Meme(i + 1, ((WIDTH - 400) / 2f - WIDTH / 16f) + packetWorldData.memes[i].x,
                            (i + 0.2f) * packetWorldData.memes[i].y + 3 * PLAYER_HEIGHT, 100f, 100f,
                            packetWorldData.memes[i].skin);

                }

                Tile[] gameTiles = new Tile[tiles.length];
                for (int i = 0; i < tiles.length; i++)
                    gameTiles[i] = new Tile(packetWorldData.tiles[i].id, (i + 0.5f) * (HEIGHT / packetWorldData.tiles[i].y) + 1.5f * PLAYER_HEIGHT, (WIDTH / 2.5f), (WIDTH / 20f) * 3.5f,
                            packetWorldData.tiles[i].right);
                world.setTileSet(gameTiles);
                world.setMemes(gameMemes);
                world.host = true;

                sendReliableMessage(serialize(packetWorldData));
                sendUserInfo(world.player.getSkin(), world.player.getUsername());

            }


        }

        @Override
        public void onDisconnectedFromRoom(Room room) {
            System.out.println("Disconnected..");
            game.displayError("Disconnected..");
            participants.clear();
            if (world != null)
                world.reset();
        }

        @Override
        public void onPeersConnected(Room room, List<String> list) {
        }

        @Override
        public void onPeersDisconnected(Room room, List<String> list) {
            game.displayError("Player disconnected!");
        }

        @Override
        public void onP2PConnected(String s) {

        }

        @Override
        public void onP2PDisconnected(String s) {

        }

        @Override
        public void onRoomCreated(int status, Room room) {
            if (status != GamesStatusCodes.STATUS_OK) {
                game.displayError("Could not create room!");
            } else {
                mRoomID = room.getRoomId();
                Intent intent = Games.RealTimeMultiplayer.getWaitingRoomIntent(client, room, MIN_PLAYERS);
                this.activity.startActivityForResult(intent, RC_WAITING_ROOM);

                createWorld();
                game.setScreen(world);
            }
        }

        @Override
        public void onJoinedRoom(int i, Room room) {

        }

        @Override
        public void onLeftRoom(int status, String s) {
            game.displayError("Player has left");
//            game.setScreen(new GameScreen());
        }

        @Override
        public void onRoomConnected(int i, Room room) {
            mRoomID = room.getRoomId();
        }


        private List<String> sortParticipants(ArrayList<Participant> participants, String myID) {
            List<String> ids = new ArrayList<String>();
            ids.add(myID);
            for (Participant p : participants)
                ids.add(p.getParticipantId());
            Collections.sort(ids);
            return ids;
        }

        public void sendUserInfo(String skin, String username) {
            System.out.println("Sending skin: " + skin);

            PacketUserInfo pack = new PacketUserInfo();
            pack.skin = skin;
            pack.username = username;
            pack.packetID = PACKET_USERINFO;
            try {
                byte[] packet = serialize(pack);
                sendReliableMessage(packet);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void sendPos(Vector2 pos, boolean flip) {
            try {
//            byte[] packet = ByteBuffer.allocate(16).putInt(0, PACKET_POSITION).putFloat(x).putFloat(y).array();
                PacketPosition packetPos = new PacketPosition();
                packetPos.pos = pos;
                packetPos.flip = flip;
                packetPos.packetID = PACKET_POSITION;
                byte[] packet = serialize(packetPos);
                Games.RealTimeMultiplayer.sendUnreliableMessageToOthers(client, packet, mRoomID);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void sendStatus(int score, int memes, boolean dead) {
            PacketStatus packetStats = new PacketStatus();
            packetStats.SCORE = score;
            packetStats.MEMES = memes;
            packetStats.dead = dead;
            packetStats.packetID = PACKET_STATUS;
            try {
                byte[] packet = serialize(packetStats);
                sendReliableMessage(packet);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public Tile[] generateTiles() {
            Tile[] tiles = new Tile[MathUtils.random(40, 120)];
            for (int i = tiles.length - 1; i >= 0; i--) {
                if (i == tiles.length - 1)
                    tiles[i] = new Tile(i, MathUtils.random(3.45f, 3.65f), (1920 / 2.5f), (1920 / 20f) * 3.5f,
                            MathUtils.randomBoolean(0.8f));
                else {
                    Tile prevTile = tiles[i + 1];
                    tiles[i] = new Tile(i, MathUtils.random(3.45f, 3.65f), (1920 / 2.5f), (1920 / 20f) * 3.5f,
                            MathUtils.randomBoolean(prevTile.right ? 0.2f : 0.8f));
                }
            }
            return tiles;
        }

        public Meme[] generateMemes(int numTiles) {
            int WIDTH = game.WIDTH;
            Meme[] memes = new Meme[MathUtils.random(numTiles, numTiles * 2)];
            for (int i = 0; i < memes.length; i++)
                memes[i] = new Meme(i + 1, MathUtils.random(300, 340),
                        MathUtils.random(WIDTH * 4, WIDTH * 8), 100f, 100f,
                        "meme" + MathUtils.random(1, 4));
            return memes;
        }


        public byte[] serialize(Object obj) {
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            try {
                ObjectOutputStream o = new ObjectOutputStream(b);
                o.writeObject(obj);
            } catch (Exception e) {
            }
            return b.toByteArray();
        }

        public Object deserialize(byte[] bytes) {
            try {
                ByteArrayInputStream b = new ByteArrayInputStream(bytes);
                ObjectInputStream input = new ObjectInputStream(b);
                return input.readObject();
            } catch (Exception e) {
            }
            return null;
        }

        private void sendReliableMessage(byte[] packet) {
            for (Participant p : participants)
                if (!p.getParticipantId().equals(myID))
                    Games.RealTimeMultiplayer.sendReliableMessage(client, null, packet, mRoomID, p.getParticipantId());
        }

        @Override
        public void onInvitationReceived(final Invitation invitation) {
            System.out.println("I got an invite");
        }


        @Override
        public void onInvitationRemoved(String s) {

        }

        public void sendStatus(int score, int memes, boolean dead, int id) {
            PacketStatus packetStats = new PacketStatus();
            packetStats.SCORE = score;
            packetStats.MEMES = memes;
            packetStats.dead = dead;
            packetStats.usedMeme = id;
            packetStats.packetID = PACKET_STATUS;
            try {
                byte[] packet = serialize(packetStats);
                sendReliableMessage(packet);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void reset() {
            participants.clear();
            world.players.clear();
        }

        public void invitationInbox() {

            Intent intent = Games.Invitations.getInvitationInboxIntent(client);
            this.activity.startActivityForResult(intent, RC_INVITATION_INBOX);
        }
    }
}
