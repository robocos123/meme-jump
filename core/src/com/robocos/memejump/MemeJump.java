package com.robocos.memejump;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.robocos.memejump.googleplay.ActionResolver;
import com.robocos.memejump.screens.GameScreen;
import com.robocos.memejump.screens.MultiplayerScreen;
import com.robocos.memejump.screens.Screen;
import com.robocos.memejump.screens.SplashScreen;
import com.robocos.memejump.util.GameResources;

public class MemeJump extends ApplicationAdapter {
    public static MemeJump INSTANCE;
    public Screen screen;
    public static Preferences prefs;
    public static ActionResolver gmsClient;
    public static int WIDTH, HEIGHT;
    public static String VERSION = "1.3.1";

    public String username, skin;
    public static GameResources gameResources;
    public MultiplayerScreen world;

    public MemeJump(ActionResolver gmsClient) {
        INSTANCE = this;
        MemeJump.gmsClient = gmsClient;
    }

    @Override
    public void create() {
        gameResources = new GameResources();
        screen = new SplashScreen(this);
        prefs = Gdx.app.getPreferences("MemeJump");

        WIDTH = Gdx.graphics.getWidth();
        HEIGHT = Gdx.graphics.getHeight();

        gameResources.load(screen.cam);

        gmsClient.loginGPGS();
        if (gmsClient.isConnected()) {
            username = gmsClient.getUsername();

            String[] characters = {"pepesad", "doge", "spurdo", "wojak", "spongegar", "polandball", "harambe"};
            boolean[] unlocked = new boolean[characters.length];
            int selectedChar = prefs.getInteger("selectedChar");
            unlocked[0] = true;
            for (int i = 1; i < unlocked.length; i++)
                unlocked[i] = MemeJump.prefs.getBoolean(characters[i]);
            if (unlocked[selectedChar])
                skin = characters[selectedChar];
            else
                skin = characters[0];

            MemeJump.gmsClient.checkInvites();
        }
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        screen.update(Gdx.graphics.getDeltaTime());
        screen.render();
    }

    public static void unlockAchievementGPGS(String achievement) {
        if (!MemeJump.gmsClient.achievementUnlocked(achievement)) {
            getGameResources().victory_screech.play();
            MemeJump.gmsClient.unlockAchievementGPGS(achievement);
        }
    }

    public void displayError(String s) {
        if (screen != null) {
            if (screen instanceof GameScreen) {
                GameScreen gameScreen = (GameScreen) screen;
                if (gameScreen.multiMode && gameScreen.stage.getActors().size <= 0)
                    gameScreen.setError(s);
            } else if (screen instanceof MultiplayerScreen) {
                MultiplayerScreen multiScreen = (MultiplayerScreen) screen;
                multiScreen.setError(s);
            }
        }
    }

    public void setScreen(Screen screen) {
        this.screen = screen;
    }

    @Override
    public void resize(int width, int height) {
        screen.resize(width, height);
    }

    @Override
    public void resume() {
        super.resume();
        getGameResources().load(screen.cam);
        screen.reload();
    }


    public static GameResources getGameResources() {
        return gameResources;
    }

    /*public void createWorld() {
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            // process the result, e.g. add it to an Array<Result> field of the ApplicationListener.
                            world = new MultiplayerScreen(skin, username);
                            setScreen(world);
                            gmsClient.setWorld(world);
                        }
                    });
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    */


}


