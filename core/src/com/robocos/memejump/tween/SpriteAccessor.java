package com.robocos.memejump.tween;

import com.badlogic.gdx.graphics.g2d.Sprite;

import aurelienribon.tweenengine.TweenAccessor;

/**
 * Created by Waleed on 8/30/2016.
 */
public class SpriteAccessor implements TweenAccessor<Sprite> {

    public static final int FLIP = 0, SCALE = 1, ALPHA = 2;

    @Override
    public int getValues(Sprite target, int tweenType, float[] returnValues) {
        switch(tweenType) {
            case FLIP:
                returnValues[0] = 1;
                return 1;

            case SCALE:
                returnValues[0] = target.getWidth() * 0.2f;
                returnValues[1] = target.getHeight() * 0.2f;
                return 2;

            case ALPHA:
                returnValues[0] = target.getColor().a;
                return 1;
            default:
                return -1;
        }

    }

    @Override
    public void setValues(Sprite target, int tweenType, float[] newValues) {
        switch(tweenType) {
            case FLIP:
                target.flip(!target.isFlipX(), false);
                break;
            case SCALE:
                target.setSize(newValues[0], newValues[1]);
                target.setOrigin((target.getX() + target.getWidth()) / 2f, (target.getY() + target.getHeight()) / 2f);
                break;
            case ALPHA:
                target.setColor(1, 1, 1, newValues[0]);
                break;
            default:
                break;
        }
    }
}
