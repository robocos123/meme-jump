package com.robocos.memejump.googleplay;

import com.badlogic.gdx.math.Vector2;
import com.robocos.memejump.screens.MultiplayerScreen;

/**
 * Created by Waleed on 8/26/2016.
 */

public interface ActionResolver {

    boolean isPaused();
    boolean achievementUnlocked(String s);
    boolean getSignedInGPGS();
    boolean isConnected();
    void loginGPGS();
    void submitScoreGPGS(int score);
    void unlockAchievementGPGS(String achievementId);
    void getLeaderboardGPGS();
    void getAchievementsGPGS();
    void showAd();

    void quickGame();

    void sendPos(Vector2 pos, boolean flip);
    void sendStatus(int SCORE, int MEMES, boolean dead);

    void initMatch();

    String getUsername();

    void sendStatus(int score, int memes, boolean dead, int id);

    void leaveGame();
    void checkInvites();
}
