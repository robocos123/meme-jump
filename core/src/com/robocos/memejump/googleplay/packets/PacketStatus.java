package com.robocos.memejump.googleplay.packets;

/**
 * Created by walee on 9/25/2016.
 */

public class PacketStatus extends Packet {
    public int SCORE, MEMES;
    public boolean dead;
    public int usedMeme;
}
