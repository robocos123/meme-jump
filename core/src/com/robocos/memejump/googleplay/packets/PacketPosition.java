package com.robocos.memejump.googleplay.packets;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by walee on 9/25/2016.
 */

public class PacketPosition extends Packet {
    public Vector2 pos;
    public boolean flip;
    public float rotation;
}
