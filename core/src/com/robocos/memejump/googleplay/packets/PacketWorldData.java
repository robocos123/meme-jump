package com.robocos.memejump.googleplay.packets;

import com.robocos.memejump.entities.Meme;
import com.robocos.memejump.entities.Tile;

/**
 * Created by walee on 9/25/2016.
 */

public class PacketWorldData extends Packet {
    public Tile[] tiles;
    public Meme[] memes;
}
