package com.robocos.memejump.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;
import com.robocos.memejump.MemeJump;
import com.robocos.memejump.entities.Illuminati;
import com.robocos.memejump.entities.Meme;
import com.robocos.memejump.entities.Player;
import com.robocos.memejump.entities.PlayerMP;
import com.robocos.memejump.entities.Tile;
import com.robocos.memejump.util.GameInput;
import com.robocos.memejump.util.GameResources;
import com.robocos.memejump.util.ShakingCam;
import com.sun.crypto.provider.HmacSHA1KeyGenerator;
import com.sun.org.apache.xpath.internal.axes.WalkingIterator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by walee on 9/23/2016.
 */

public class MultiplayerScreen extends Screen {

    private String error;
    public ArrayList<PlayerMP> players;
    public Player player;
    public Sound deathSound;
    public Illuminati illuminati;
    public Tile[] tiles;
    public Meme[] memes;

    private ShakingCam shakeCam;
    private Texture ground;


    //playing means nothing is loaded but there is a connection
    public boolean disconnected, gameOver, playing;
    private long tapTime;
    public boolean host;
    public boolean setTiles, setMemes;

    public MultiplayerScreen(Screen prevScreen, String skin, String username) {
        getScreenInfo(prevScreen);

        players = new ArrayList<PlayerMP>();
        player = new Player(MathUtils.random(WIDTH / 8f, WIDTH - (WIDTH / 20) * 2.5f), 20, (WIDTH / 20f) * 2f, (WIDTH / 20f) * 2f, skin, username);

        cam.position.x = WIDTH / 2f;
        cam.position.y = player.pos.y + 20;
        cam.update();
        batch.setProjectionMatrix(cam.combined);
        sr.setProjectionMatrix(cam.combined);

        bigFont.setColor(Color.WHITE);
        smallerFont.setColor(Color.WHITE);
        smallerFont.setColor(Color.WHITE);
        regularFont.setColor(Color.WHITE);

        error = "Disconnected!";
        ground = MemeJump.getGameResources().TILE;
        deathSound = MathUtils.randomBoolean() ? MemeJump.getGameResources().scream : MemeJump.getGameResources().triggered;
        shakeCam = new ShakingCam(15, 2);

        illuminati = new Illuminati(WIDTH / 4, -3000, WIDTH / 2f, WIDTH / 2f);
        MemeJump.gameResources.nicememe.stop();
        MemeJump.gameResources.nicememe.setLooping(false);
    }

    private void getScreenInfo(Screen prevScreen) {
       WIDTH = prevScreen.WIDTH;
        HEIGHT = prevScreen.HEIGHT;
        cam = prevScreen.cam;
        batch = prevScreen.batch;
        batch.setProjectionMatrix(cam.combined);

        sr = prevScreen.sr;
        sr.setProjectionMatrix(cam.combined);

        LIGHT_BLUE = new Color(173 / 255f, 216 / 255f, 230 / 255f, 1.0f);
        MID_BLUE = new Color(0 / 255f, 195 / 255f, 255 / 255f, 1.0f);
        DARK_BLUE = new Color(25 / 255f, 25 / 255f, 112 / 255f, 1.0f);

        bigFont = MemeJump.getGameResources().bigFont;
        regularFont = MemeJump.getGameResources().regularFont;
        smallerFont = MemeJump.getGameResources().smallerFont;
        smallFont = MemeJump.getGameResources().smallFont;
    }

    public PlayerMP addPlayer(String name, String id, String skin) {
        System.out.println(skin);
        PlayerMP newPlayer = new PlayerMP(MathUtils.random(WIDTH / 10.8f, WIDTH - WIDTH / 10.8f), 20, (WIDTH / 20f) * 2f, (WIDTH / 20f) * 2f, skin,
                name, id);
        players.add(newPlayer);
        return newPlayer;
    }


    @Override
    public void render() {
        sr.begin(ShapeRenderer.ShapeType.Filled);
        sr.rect(-1000, -1000, 10 * WIDTH, 10 * HEIGHT, LIGHT_BLUE, MID_BLUE, DARK_BLUE, Color.BLACK);
        sr.end();

        bigFont.setColor(Color.WHITE);
        if (!disconnected) {
            batch.begin();
            batch.draw(ground, WIDTH, 0, -3000, -3000);
            batch.end();

            playing = isPlaying();
            if (playing) {
                batch.begin();
                if (player != null && !player.dead)
                    player.render(batch);
                for (PlayerMP playerMP : players)
                    if (playerMP != null && !playerMP.dead) {
                        smallFont.setColor(Color.GREEN);
                        smallFont.draw(batch, playerMP.username, playerMP.pos.x, playerMP.pos.y + playerMP.height);
                        playerMP.render(batch);
                    }

                if (tiles != null)
                    for (Tile tile : tiles)
                        tile.render(batch);

                if (memes != null)
                    for (Meme meme : memes)
                        if (!meme.used)
                            meme.render(batch);

                batch.end();
                if (player.dead) {
                    MemeJump.getGameResources().nicememe.stop();
                    if (shakeCam.elapsed > shakeCam.duration) {
                        drawOverlay(new Color(0, 0, 0, 0.3f));
                        batch.begin();
                        smallerFont.setColor(Color.RED);
                        smallerFont.draw(batch, "Spectating " + getFirstPlace().username, WIDTH / (getFirstPlace().username.length() / 1.5f), cam.position.y + cam.viewportHeight / 4f);
                        batch.end();
                    }
                } else {
                    batch.begin();
                    bigFont.draw(batch, player.SCORE + "", 0.5f * (cam.viewportWidth / 2.75f + cam.viewportWidth / 1.5f), cam.position.y + HEIGHT / 2.4f);
                    smallFont.setColor(Color.WHITE);
                    smallFont.draw(batch, "MEMES: " + player.MEMES, cam.viewportWidth / 20.15f, cam.position.y + HEIGHT / 2.5f);
                    smallFont.draw(batch, "1st Place: " + (getFirstPlace().SCORE > player.SCORE || player.dead ? getFirstPlace().username : player.username), cam.viewportWidth / 20.15f,
                            (cam.position.y + HEIGHT / 2.5f) - 50);
                    batch.end();
                }
            } else {
                if (gameOver) {
                    if (player != null) {
                        boolean playerMPWon = getFirstPlace().SCORE > player.SCORE;
                        drawOverlay(playerMPWon ? new Color(1.0f, 0, 0, 0.5f) : new Color(0f, 0.5f, 0, 0.5f));
                        batch.begin();
                        if (playerMPWon) {
                            regularFont.setColor(Color.WHITE);
                            regularFont.draw(batch, players.get(0).username + " won", WIDTH / 10f, HEIGHT / 1.5f);
                            players.get(0).render(batch, WIDTH / 2, HEIGHT / 2);
                        } else {
                            if (player.SCORE > getFirstPlace().SCORE) {
                                regularFont.setColor(Color.GOLD);
                                regularFont.draw(batch, "YOU WON!", WIDTH / 4f, HEIGHT / 1.5f);
                            } else {
                                regularFont.setColor(Color.GRAY);
                                regularFont.draw(batch, "Tie!", WIDTH / 4f, HEIGHT / 1.5f);
                            }
                        }
                        regularFont.setColor(Color.BLACK);
                        regularFont.draw(batch, "SCORE: " + player.SCORE, WIDTH / 4f, HEIGHT / 2f);
                        regularFont.draw(batch, "MEMES: " + player.MEMES, WIDTH / 4f, HEIGHT / 3f);
                        smallerFont.setColor(Color.WHITE);
                        smallerFont.draw(batch, "Tap to quit", WIDTH / 4f, HEIGHT / 7f);
                        batch.end();
                    }
                } else {
                    batch.begin();
                    smallerFont.setColor(Color.WHITE);
                    smallerFont.draw(batch, "Waiting for players..", WIDTH / 4, HEIGHT / 4);
                    batch.end();
                }
            }
        } else {
            drawOverlay(new Color(1.0f, 0, 0, 0.5f));
            batch.begin();

            smallerFont.setColor(Color.WHITE);
            regularFont.setColor(Color.BLACK);
            regularFont.draw(batch, error, WIDTH / 2, HEIGHT / 4);
            smallerFont.draw(batch, "Tap to return", WIDTH / 4, HEIGHT / 5.5f);
            batch.end();
        }
    }

    @Override
    public void update(float delta) {
        if (!disconnected) {
            playing = isPlaying();
            if (playing) {
                if (!player.dead) {
                    cam.position.set(cam.viewportWidth / 2f, player.pos.y + 20, 0);
                    cam.update();
                    batch.setProjectionMatrix(cam.combined);
                    sr.setProjectionMatrix(cam.combined);

                    if (Gdx.input.justTouched() && player.pos.y >= 20) {
                        if (Gdx.input.getX() >= WIDTH / 2f)
                            player.velocity = new Vector2(player.width * 5f, HEIGHT / 1.35f - player.GRAVITY / 2.5f);
                        else
                            player.velocity = new Vector2(-player.width * 5f, HEIGHT / 1.35f - player.GRAVITY / 2.5f);
                        player.left = Gdx.input.getX() < WIDTH / 2f;

                    }
                    player.update(delta, this);
                    for (Tile t : tiles)
                        if (t != null && t.update) {
                            t.update(delta, this);
                            if (t.scrolledUp) {
                                player.SCORE++;
                                MemeJump.gmsClient.sendStatus(player.SCORE, player.MEMES, player.dead);
                                t.update = false;
                            }
                        }
                } else {
                    if (shakeCam.elapsed > shakeCam.duration) {
                        Vector2 winnerPos = getFirstPlace().pos;
                        cam.position.set(cam.viewportWidth / 2f, winnerPos.y + 20, 0);
                        cam.update();
                        batch.setProjectionMatrix(cam.combined);
                        sr.setProjectionMatrix(cam.combined);
//                        deathSound.stop();
                    } else {
                        MemeJump.gameResources.nicememe.stop();
                        MemeJump.gameResources.nicememe.setLooping(false);
//                        deathSound.play();
                        shakeCam.update(delta, cam);
                        cam.update();
                        sr.setProjectionMatrix(cam.combined);
                        batch.setProjectionMatrix(cam.combined);
                    }
                }
                for (PlayerMP playerMP : players)
                    if (playerMP != null && !playerMP.dead)
                        playerMP.update(delta);

                for (Meme meme : memes)
                    if (meme != null && !meme.used)
                        meme.update(delta, this);

                if(players.size() <= 0)
                    disconnected = true;
                gameOver = allPlayersDead() || getFirstPlace().SCORE == tiles.length || player.SCORE == tiles.length;
                if(gameOver)
                    tapTime = TimeUtils.millis();
            } else if (gameOver) {
                cam.position.set(cam.viewportWidth / 2f, cam.viewportHeight / 2f, 0);
                cam.update();
                batch.setProjectionMatrix(cam.combined);
                sr.setProjectionMatrix(cam.combined);
                if (Gdx.input.justTouched() && TimeUtils.timeSinceMillis(tapTime) > 1500) {
                    MemeJump.gmsClient.leaveGame();
                    MemeJump.INSTANCE.setScreen(new GameScreen());
                }
            }
        } else {
            if (Gdx.input.justTouched())
                MemeJump.INSTANCE.setScreen(new GameScreen());
        }

        if (!MemeJump.gmsClient.isConnected() && !error.equals("Could not connect!"))
            setError("Could not connect!");

        if (players.size() == 0)
            setError("Player disconnected!");
        disconnected = !isConnected();
    }

    private boolean isPlaying() {
        return isConnected() && !gameOver;
    }

    private boolean isConnected() {
        return players.size() > 0 && player != null && tiles != null && memes != null && MemeJump.gmsClient.isConnected();
    }

    private boolean allPlayersDead() {
        boolean othersDead = false;
        for (PlayerMP playerMP : players)
            othersDead = playerMP.dead;
        return othersDead && player.dead && shakeCam.duration < shakeCam.elapsed;
    }

    public void setError(String error) {
        this.error = error;
        disconnected = true;
    }

    public void setTileSet(Tile[] tileSet) {
        tiles = tileSet;
    }

    public PlayerMP getPlayerFromID(String id) {
        for (PlayerMP player : players)
            if (player.parID.equals(id))
                return player;
        return null;
    }

    private PlayerMP getFirstPlace() {
        if(players.size() > 1) {
            PlayerMP firstPlace = players.get(0);
            for(int i = 0; i < players.size(); i++) {
                if(firstPlace.SCORE < players.get(i).SCORE)
                    firstPlace = players.get(i);
            }
            return firstPlace;
        }else {
            return players.get(0);
        }
    }

    /*public Player getFirstPlace() {
        /*if (players.size() > 0 && player != null && players.get(0) != null) {
            Player firstPlace = players.get(0);
            if (players.size() > 1) {
                for (int i = 0; i < players.size(); i++) {
                    if (players.get(i) != null && players.get(i).compareTo(player) > 0) {
                        if (firstPlace.compareTo(players.get(i)) > 0 || firstPlace.dead)
                            firstPlace = players.get(i);
                    }
                }
            }
            if (firstPlace.compareTo(player) > 0 || firstPlace.dead)
                firstPlace = player;
            return firstPlace;
            }
        if(players.size() > 0) {
            if(players.get(0) != null) {
                Player firstPlace = players.get(0);
                if(firstPlace.compareTo(player) > 0)
                    return firstPlace;
                else
                    return player;
            }
        }
        return null;
    }*/

    public void reset() {
        players.clear();
    }

    private void drawOverlay(Color color) {
        Gdx.graphics.getGL20().glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        sr.begin(ShapeRenderer.ShapeType.Filled);
        sr.setColor(color);
        sr.rect(-1000, -1200,  WIDTH * 10, cam.viewportHeight / 100f + HEIGHT * 10);
        sr.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    public void setMemes(Meme[] memes) {
        this.memes = memes;
    }

    /*public Player getFirstPlace() {
        if(players.get(0) != null && players.get(0).pos != null) {
        if (player.dead || player.compareTo(players.get(0)) < 0)
            return players.get(0);
        else
            return player;
        }
        return player;
    }*/
}
