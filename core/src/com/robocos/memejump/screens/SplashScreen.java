package com.robocos.memejump.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.robocos.memejump.util.GameResources;
import com.robocos.memejump.MemeJump;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenAccessor;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

public class SplashScreen extends Screen {

    private SpriteBatch batcher;
    private Sprite sprite;
    private MemeJump game;

    public SplashScreen(MemeJump game) {
        super();
        this.game = game;

        sprite = new Sprite(new Texture(MemeJump.getGameResources().internal("gui/logo.png")));
        sprite.setColor(0 / 255.0f, 0 / 255.0f, 0 / 255.0F, 255.0f);

        float width = Gdx.graphics.getWidth();
        float height = Gdx.graphics.getHeight();
        float desiredWidth = width * .7f;
        float scale = desiredWidth / sprite.getWidth();

        sprite.setSize(sprite.getWidth() * scale, sprite.getHeight() * scale);
        sprite.setPosition((width / 2) - (sprite.getWidth() / 2), (height / 2)
                - (sprite.getHeight() / 2));
        setupTween();
        batcher = new SpriteBatch();
    }

    private void setupTween() {
        Tween.registerAccessor(Sprite.class, new SpriteAccessor());
        manager = new TweenManager();

        TweenCallback cb = new TweenCallback() {
            @Override
            public void onEvent(int type, BaseTween<?> source) {
                game.screen = new GameScreen();
            }
        };

        Tween.to(sprite, SpriteAccessor.ALPHA, .8f).target(1)
                .ease(TweenEquations.easeInOutQuad).repeatYoyo(1, 1f)
                .setCallback(cb).setCallbackTriggers(TweenCallback.COMPLETE)
                .start(manager);

    }

    public void update(float delta) {
        manager.update(delta);
    }

    public void render() {
        Gdx.graphics.getGL20().glClearColor(160 / 255.0f, 0 / 255.0f, 1 / 255.0F, 255.0f);
        Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        batcher.begin();
        sprite.draw(batcher);
        batcher.end();
    }

    class SpriteAccessor implements TweenAccessor<Sprite> {

        public static final int ALPHA = 1;

        @Override
        public int getValues(Sprite target, int tweenType, float[] returnValues) {
            switch (tweenType) {
                case ALPHA:
                    returnValues[0] = target.getColor().a;
                    return 1;
                default:
                    return 0;
            }
        }

        @Override
        public void setValues(Sprite target, int tweenType, float[] newValues) {
            switch (tweenType) {
                case ALPHA:
                    target.setColor(1, 1, 1, newValues[0]);
                    break;
            }
        }

    }

}
