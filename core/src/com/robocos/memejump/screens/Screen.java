package com.robocos.memejump.screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.robocos.memejump.MemeJump;
import com.robocos.memejump.util.GameResources;

import aurelienribon.tweenengine.TweenManager;

/**
 * Created by Waleed on 8/14/2016.
 */
public abstract class Screen {

    public float WIDTH, HEIGHT;

    public OrthographicCamera cam;
    public SpriteBatch batch;
    public BitmapFont bigFont, regularFont, smallerFont, smallFont;
    public TweenManager manager;

    public ShapeRenderer sr;
    public Color LIGHT_BLUE, MID_BLUE, DARK_BLUE;


    public Screen() {
        WIDTH = Gdx.graphics.getWidth();
        HEIGHT = Gdx.graphics.getHeight();

        cam = new OrthographicCamera(WIDTH / 2, HEIGHT / 2);
        cam.setToOrtho(false, WIDTH, HEIGHT);
        cam.translate(WIDTH / 4, HEIGHT / 4);

        batch = new SpriteBatch();
        batch.setProjectionMatrix(cam.combined);
        sr = new ShapeRenderer();
        sr.setProjectionMatrix(cam.combined);

        LIGHT_BLUE = new Color(173 / 255f, 216 / 255f, 230 / 255f, 1.0f);
        MID_BLUE = new Color(0 / 255f, 195 / 255f, 255 / 255f, 1.0f);
        DARK_BLUE = new Color(25 / 255f, 25 / 255f, 112 / 255f, 1.0f);

        bigFont = MemeJump.getGameResources().bigFont;
        regularFont = MemeJump.getGameResources().regularFont;
        smallerFont = MemeJump.getGameResources().smallerFont;
        smallFont = MemeJump.getGameResources().smallFont;
    }


    public abstract void render();

    public abstract void update(float delta);

    public void reload() {
        if (this instanceof MultiplayerScreen) {
            WIDTH = Gdx.graphics.getWidth();
            HEIGHT = Gdx.graphics.getHeight();

            batch = new SpriteBatch();
            batch.setProjectionMatrix(cam.combined);

            sr = new ShapeRenderer();
            sr.setProjectionMatrix(cam.combined);

            LIGHT_BLUE = new Color(173 / 255f, 216 / 255f, 230 / 255f, 1.0f);
            MID_BLUE = new Color(0 / 255f, 195 / 255f, 255 / 255f, 1.0f);
            DARK_BLUE = new Color(25 / 255f, 25 / 255f, 112 / 255f, 1.0f);

            bigFont = MemeJump.getGameResources().bigFont;
            regularFont = MemeJump.getGameResources().regularFont;
            smallerFont = MemeJump.getGameResources().smallerFont;
            smallFont = MemeJump.getGameResources().smallFont;


        }
    }

    public void resize(int width, int height) {
//        if(batch != null) {
//            batch.getProjectionMatrix().setToOrtho2D(cam.pos, 0, WIDTH, HEIGHT);
//        }
    }
}
