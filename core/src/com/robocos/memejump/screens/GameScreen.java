package com.robocos.memejump.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.robocos.memejump.tween.ActorAccessor;
import com.robocos.memejump.util.GameInput;
import com.robocos.memejump.util.GameResources;
import com.robocos.memejump.MemeJump;
import com.robocos.memejump.util.ShakingCam;
import com.robocos.memejump.entities.Illuminati;
import com.robocos.memejump.entities.Meme;
import com.robocos.memejump.entities.Player;
import com.robocos.memejump.entities.Tile;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

import com.robocos.memejump.tween.SpriteAccessor;


/**
 * Created by Waleed on 8/14/2016.
 */
public class GameScreen extends Screen {

    private Timeline t1;
    private Rectangle boardBounds;
    private long nextClick;
    public Sound deathSound;
    public boolean loaded, disconnected, multiMode, singleMode, started;
    private String error;

    public int HIGHSCORE;

    public String getSelectedCharacter() {
        return characters[selectedChar];
    }

    public enum State {
        TITLE,
        PLAY,
        GAME_OVER,
        INVITATION
    }

    private GameInput input;

    public String[] characters = {"pepesad", "doge", "spurdo", "wojak", "spongegar", "datboi", "polandball", "harambe"};
    public String[] names = {"Pepe", "Doge", "Spurdo", "Wojak", "Spongegar", "Dat Boi", "Polandball", "Harambe"};
    public int[] prices = {0, 250, 420, 500, 700, 750, 800, 1000};
    public boolean[] unlocked;

    public State state;

    public Player player;
    public Illuminati illuminati;
    public Texture ground;
    public Tile[] tiles;
    public Meme[] memes;

    private ShakingCam shake;

    public TextButton[] buttons;

    public Stage stage;
    public Actor board;

    public Sprite selected, next, prev;
    public ImageButton arrowLeft, arrowRight;

    public int selectedChar;
    private boolean store;

    public GameScreen() {
        super();

        MemeJump.gameResources.nicememe.stop();
        input = new GameInput(this);
        error = "";

        deathSound = MathUtils.randomBoolean() ? MemeJump.getGameResources().triggered : MemeJump.getGameResources().scream;

        selectedChar = MemeJump.prefs.getInteger("selectedChar");

        player = new Player(WIDTH / 2, 20, (WIDTH / 20f) * 2f, (WIDTH / 20f) * 2f, characters[selectedChar]);
        illuminati = new Illuminati(WIDTH / 4, HEIGHT / (HEIGHT / -1000), WIDTH / 2f, WIDTH / 2f);
        ground = MemeJump.getGameResources().TILE;
        initSprites();

        unlocked = new boolean[characters.length];

        unlocked[0] = true;
        for (int i = 1; i < unlocked.length; i++)
            unlocked[i] = MemeJump.prefs.getBoolean(characters[i]);

        stage = new Stage(new ExtendViewport(WIDTH, HEIGHT));

        shake = new ShakingCam(15, 2);
        state = State.TITLE;

        player.MEMES = MemeJump.prefs.getInteger("memes");
        HIGHSCORE = MemeJump.prefs.getInteger("highscore");
        illuminati.kills = MemeJump.prefs.getInteger("nwo");

        buttons = new TextButton[3];

        board = new Image(MemeJump.getGameResources().buttons.getRegion("market"));

        ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle();
        style.imageUp = MemeJump.getGameResources().buttons.getDrawable("arrow_left");
        arrowLeft = new ImageButton(style);
        arrowLeft.getImage().setSize(3.7f * WIDTH, 3.7f * WIDTH);

        ImageButton.ImageButtonStyle style2 = new ImageButton.ImageButtonStyle();
        style2.imageUp = MemeJump.getGameResources().buttons.getDrawable("arrow_right");
        arrowRight = new ImageButton(style2);
        arrowRight.getImage().setSize(3.7f * WIDTH, 3.7f * WIDTH);

        cam.position.x = cam.viewportWidth / 2f;
        cam.position.y = player.pos.y + 20;
        cam.update();

        Tween.registerAccessor(Sprite.class, new SpriteAccessor());
        Tween.registerAccessor(Actor.class, new ActorAccessor());
        manager = new TweenManager();

        initMenuButtons();


        Gdx.input.setInputProcessor(stage);

        if (!MemeJump.gameResources.nicememe.isPlaying()) {
            MemeJump.gameResources.nicememe.play();
            MemeJump.gameResources.nicememe.setLooping(true);
        }

    }

    private void initSprites() {
        tiles = new Tile[5];
        for (int i = 0; i < tiles.length; i++)
            tiles[i] = new Tile(this, i, (i + 0.5f) * (HEIGHT / (MathUtils.random(3.45f, 3.65f))) + 1.5f * player.height, (WIDTH / 2.5f), (WIDTH / 20f) * 3.5f, MathUtils.randomBoolean(0.3f));

        memes = new Meme[3];
        for (int i = 0; i < memes.length; i++)
            memes[i] = new Meme(this, (i + 0.2f) * MathUtils.random(WIDTH * 4, WIDTH * 8) + 3 * player.height, 100, 100,
                    "meme" + MathUtils.random(1, 4));
    }

    private void initMenuButtons() {
        stage.clear();
        singleMode = false;
        input = new GameInput(this);

        buttons = new TextButton[2];
        buttons[0] = createButton(WIDTH / 4 + WIDTH / 14.16f, HEIGHT / 2f, "Play", "singleplayer");
        buttons[0].addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                singleMode = true;
                stage.clear();
            }
        });
        buttons[1] = createButton(WIDTH / 4 - WIDTH / 10.8f, buttons[0].getY() - buttons[0].getHeight(), "Multiplayer", "multiplayer");
        buttons[1].setWidth(buttons[1].getWidth() * 1.8f);
        buttons[1].addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                initMultiButtons();
                multiMode = true;
            }
        });
        for (Actor but : buttons)
            stage.addActor(but);
    }

    private void initMultiButtons() {
        stage.clear();
        buttons = new TextButton[2];
        buttons[0] = createButton(WIDTH / 4, (HEIGHT / 4.57f) + cam.viewportHeight / 4f - HEIGHT / 5.5f, "Quick Play", "singleplayer");
        buttons[0].setWidth(buttons[0].getWidth() * 1.5f);
        buttons[0].addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (MemeJump.gmsClient.isConnected()) {
                    MemeJump.gmsClient.quickGame();
                    stage.clear();
                } else {
                    setError("Not signed in");
                }

            }
        });
        buttons[1] = createButton(WIDTH / 4 + WIDTH / 10, 2 * (HEIGHT / 4.57f) + cam.viewportHeight / 4f - HEIGHT / 5.5f, "Host", "multiplayer");
        buttons[1].addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (MemeJump.gmsClient.isConnected()) {
                    MemeJump.gmsClient.initMatch();
                    stage.clear();
                } else {
                    setError("Not signed in");
                }
//                setError("Still in development");
            }
        });
        for (Actor but : buttons)
            stage.addActor(but);
    }

    private void initGameOverMenu() {
        stage.clear();
        buttons = new TextButton[3];
        buttons[0] = createButton(2, "Restart", "restart");
//        buttons[0].setWidth(buttons[0].getWidth() * 1.15f);
        buttons[0].addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                try {
                    Thread.sleep(30);
                } catch (Exception e) {
                }
                resetGame();
            }
        });

        buttons[1] = createButton(1, "Leaderboards", "leaderboard");
        buttons[1].setX(WIDTH / 4f - 60);
        buttons[1].setWidth(buttons[1].getWidth() * 1.8f);
        buttons[1].addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (MemeJump.gmsClient.isConnected())
                    MemeJump.gmsClient.getLeaderboardGPGS();
                else {
                    disconnected = true;
                    error = "Not signed in!";
//                    MemeJump.gmsClient.loginGPGS();
                }

            }
        });
        buttons[1].setOrigin((buttons[1].getX() + buttons[1].getWidth()) / 2f, (buttons[1].getY() + buttons[1].getHeight()) / 2f);


        buttons[2] = createButton(0, "Select", "store");
//        buttons[2].setX(WIDTH / 4f - 60);
//        buttons[2].setWidth(buttons[2].getWidth() * 2f);
        buttons[2].addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                try {
                    Thread.sleep(10);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                createStore();
            }
        });
//        buttons[2].setWidth(buttons[2].getWidth() * 1.8f);
        buttons[2].setOrigin((buttons[2].getX() + buttons[2].getWidth()) / 2f, (buttons[2].getY() + buttons[2].getHeight()) / 2f);

        for (Actor but : buttons)
            stage.addActor(but);

    }

    private void createStore() {
        stage.clear();

        createItems();

        board.setPosition(-50, HEIGHT / 3.5f);
        board.setSize(WIDTH + 250, HEIGHT / 3f);
        boardBounds = new Rectangle(board.getX(), cam.position.y - HEIGHT / 3.1f, board.getWidth(),
                board.getHeight() + 200);


        arrowLeft.setPosition(prev.getX() - WIDTH / 4.9f, board.getY() + HEIGHT / 12f);
        arrowLeft.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (TimeUtils.timeSinceMillis(nextClick) > 300) {
                    nextClick = TimeUtils.millis();
                    selectCharacter(true);
                }
            }
        });
        arrowLeft.setSize(HEIGHT / 7.5f, HEIGHT / 7.5f);

        arrowRight.setPosition(WIDTH * 3 / 4f, board.getY() + HEIGHT / 12f);
        arrowRight.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (TimeUtils.timeSinceMillis(nextClick) > 300) {
                    nextClick = TimeUtils.millis();
                    selectCharacter(false);
                }
            }
        });
        arrowRight.setSize(HEIGHT / 7.5f, HEIGHT / 7.5f);


        stage.addActor(board);
        stage.addActor(arrowLeft);
        stage.addActor(arrowRight);
        cam.position.x = cam.viewportWidth / 2f;
        cam.update();

        batch.setProjectionMatrix(cam.combined);
        sr.setProjectionMatrix(cam.combined);

        store = true;
    }

    private void selectCharacter(boolean left) {
        selectedChar = left ? (selectedChar > 0 ? selectedChar - 1 : characters.length - 1) : (selectedChar + 1) % characters.length;

        TextureRegion currentSkin = MemeJump.getGameResources().sprites.getRegion(characters[selectedChar]);
        selected.setRegion(currentSkin);
        selected.setSize(HEIGHT / 7.5f, HEIGHT / 7.5f);

        if (!unlocked[selectedChar])
            Tween.to(selected, SpriteAccessor.ALPHA, .8f).targetRelative(0.4f)
                    .ease(TweenEquations.easeInOutQuad).targetRelative(1.0f).repeat(Tween.INFINITY, 0.3f)
                    .start(manager);
        else {
            selected.setAlpha(1.0f);
            manager.killTarget(selected, SpriteAccessor.ALPHA);
        }

        TextureRegion prevSkin = MemeJump.getGameResources().sprites.getRegion(characters[previousItem()]);
        prev.setRegion(prevSkin);
        prev.setSize(HEIGHT / 12f, HEIGHT / 12f);

        TextureRegion nextSkin = MemeJump.getGameResources().sprites.getRegion(characters[nextItem()]);
        next.setRegion(nextSkin);
        next.setSize(HEIGHT / 12f, HEIGHT / 12f);
    }

    private void createItems() {

        selected = new Sprite(MemeJump.getGameResources().sprites.getRegion(characters[selectedChar]));
        selected.setSize(HEIGHT / 7.5f, HEIGHT / 7.5f);
        selected.setPosition(WIDTH / 4f + WIDTH / 8f, (cam.position.y + HEIGHT / 20f) - HEIGHT / 5.45f);
        selected.setBounds(selected.getX(), selected.getY(), selected.getWidth(), selected.getHeight());
        selected.setOrigin((selected.getX() + selected.getWidth()) / 2f, (selected.getY() + selected.getHeight()) / 2f);

        prev = new Sprite(MemeJump.getGameResources().sprites.getRegion(characters[previousItem()]));
        prev.setSize(HEIGHT / 12f, HEIGHT / 12f);
        prev.setPosition(selected.getX() - WIDTH / 5.75f, selected.getY());

        next = new Sprite(MemeJump.getGameResources().sprites.getRegion(characters[nextItem()]));
        next.setSize(HEIGHT / 12f, HEIGHT / 12f);
        next.setPosition(selected.getX() + WIDTH / 3.75f, selected.getY());
    }


    private TextButton createButton(int id, String text, String name) {
        TextButton button = createButton(WIDTH / 4f + WIDTH / 9f, id * (HEIGHT / 4.57f) + cam.viewportHeight / 4f - HEIGHT / 8f,
                text, name);
        return button;
    }

    private TextButton createButton(float x, float y, String text, String name) {
        TextButton.TextButtonStyle style = new TextButton.TextButtonStyle();
        style.font = regularFont;
        style.up = MemeJump.getGameResources().buttons.getDrawable(name);
        TextButton button = new TextButton(text, style);
        button.setPosition(x, y);
        button.setSize(WIDTH / 2.7f, HEIGHT / 4.8f);
        return button;
    }

    @Override
    public void render() {
        sr.begin(ShapeRenderer.ShapeType.Filled);
        sr.rect(-1000, -1000, 10 * WIDTH, 10 * HEIGHT, LIGHT_BLUE, MID_BLUE, DARK_BLUE, Color.BLACK);
        sr.end();

        switch (state) {
            case TITLE:
                bigFont.setColor(new Color(65 / 255f, 104 / 255f, 37 / 255f, 1.0f));
                regularFont.setColor(new Color(65 / 255f, 104 / 255f, 37 / 255f, 1.0f));
                smallFont.setColor(Color.WHITE);

                drawWorld();
                if (disconnected) {
                    drawOverlay(new Color(0, 0, 0, 0.5f));

                    batch.begin();
                    regularFont.setColor(Color.RED);
                    regularFont.draw(batch, error, WIDTH / 10f, cam.position.y + HEIGHT / 3.5f);
                    regularFont.setColor(Color.WHITE);
                    regularFont.draw(batch, "Tap to return", WIDTH / 6f, cam.viewportHeight / 8f);
                    batch.end();
                } else {
                    if (!multiMode) {
                        batch.begin();
                        if (singleMode)
                            regularFont.draw(batch, "Tap to start", cam.viewportWidth / 4f, HEIGHT / 4f);
                        else {
                            bigFont.draw(batch, "Meme Jump", cam.viewportWidth / 12f, HEIGHT / 2.8f);
//                            smallerFont.draw(batch, "V: " + MemeJump.VERSION, WIDTH - WIDTH / 3.4f, -1500);
                        }
                        batch.end();
                    } else {
                        drawOverlay(new Color(0, 0.5f, 0, 0.5f));
                        batch.begin();
                        bigFont.setColor(Color.WHITE);
                        smallerFont.setColor(Color.WHITE);
                        if (stage.getActors().size > 0) {
                            bigFont.draw(batch, "Multiplayer", cam.viewportWidth / 20f, HEIGHT / 2.4f);
                            smallerFont.draw(batch, "Tap anywhere to return", cam.viewportWidth / 7f, HEIGHT / -4.9f);
                            smallFont.setColor(Color.ORANGE);
                            smallFont.draw(batch, "NOTE: Multiplayer is still in development!", WIDTH / 4f, HEIGHT / -3.75f);
                            smallFont.draw(batch, "If any bugs occur, please contact me!", WIDTH / 3.5f, HEIGHT / -3.25f);
                        } else {
                            bigFont.draw(batch, "Loading..", cam.viewportWidth / 4, HEIGHT / 4f);
                            smallerFont.setColor(Color.RED);
                            smallerFont.draw(batch, "Tap to quit", cam.viewportWidth / 4f + cam.viewportWidth / 10.8f, HEIGHT / 7.5f);
                        }
                        batch.end();
                    }
                    stage.draw();
                }

                break;
            case PLAY:
                bigFont.setColor(Color.WHITE);
                smallFont.setColor(Color.WHITE);
                drawWorld();
                drawScore();
                break;
            case GAME_OVER:
                drawWorld();
                smallerFont.setColor(Color.WHITE);
                if (!disconnected) {
                    if (store) {
                        regularFont.setColor(Color.WHITE);
                        drawOverlay(new Color(0, 0, 0, 0.5f));

                        batch.begin();
                        bigFont.setColor(Color.WHITE);
                        bigFont.draw(batch, "Character Select", WIDTH / 12f, cam.position.y + HEIGHT - 50f);
                        batch.end();
                    }

                    drawScore();
                    stage.draw();
                    if (store)
                        drawItems();
                } else {
                    drawOverlay(new Color(0, 0, 0, 0.5f));

                    batch.begin();
                    regularFont.setColor(Color.RED);
                    regularFont.draw(batch, error, WIDTH / 12f, cam.position.y + cam.viewportHeight / 3.5f);
                    regularFont.setColor(Color.WHITE);
                    regularFont.draw(batch, "Tap to return", WIDTH / 4f, cam.viewportHeight / 8f);
                    batch.end();
                }
                break;

            case INVITATION:
                drawOverlay(new Color(0, 0, 0.5f, 0.5f));
                smallerFont.setColor(Color.WHITE);
                batch.begin();
                smallerFont.draw(batch, "You have been invited to a match", WIDTH / 10f, WIDTH / 4f);
                batch.end();
                break;
        }
    }

    private void drawWorld() {
        batch.begin();
        batch.draw(ground, WIDTH, 0, -3000, -3000);
        for (Tile t : tiles)
            t.render(batch);
        if (!(player.dead && state == State.GAME_OVER))
            player.render(batch);

        for (Meme meme : memes)
            meme.render(batch);

        illuminati.render(batch);
        batch.end();
    }

    private void drawScore() {
        batch.begin();
        if (!store) {
            bigFont.draw(batch, player.SCORE + "", 0.5f * (cam.viewportWidth / 2.75f + cam.viewportWidth / 1.5f), cam.position.y + HEIGHT / 2.4f);
            smallFont.draw(batch, "HIGHSCORE: " + HIGHSCORE, cam.viewportWidth / 20.15f, cam.position.y + HEIGHT / 2.5f);
            smallFont.draw(batch, "MEMES: " + player.MEMES, cam.viewportWidth / 20.15f, (cam.position.y + HEIGHT / 2.5f) - 50);
        } else {
            smallFont.draw(batch, "MEMES: " + player.MEMES, WIDTH / 4f + 90, (cam.position.y + HEIGHT / 5.5f) - 50);
            regularFont.draw(batch, "Character Selection", WIDTH / 32f, cam.position.y + HEIGHT / 2.8f);
            smallerFont.setColor(Color.WHITE);
            smallerFont.draw(batch, "Tap anywhere to return", WIDTH / 8f, cam.position.y - HEIGHT / 3f);
        }
        batch.end();
    }

    private void drawOverlay(Color color) {
        Gdx.graphics.getGL20().glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        sr.begin(ShapeRenderer.ShapeType.Filled);
        sr.setColor(color);
        sr.rect(-1000, -1200, WIDTH * 10, HEIGHT * 10);
        sr.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    private void drawItems() {
        batch.begin();
        /*batch.draw(selected.getTexture(), selected.getX(), selected.getY(), selected.getOriginX(), selected.getOriginY(),
                selected.getWidth(), selected.getHeight(), selected.getScaleX(), selected.getScaleY(), selected.getRotation(),
                selected.getRegionX(), selected.getRegionY(), selected.getRegionWidth(), selected.getRegionHeight(), false, false);
        */
        selected.draw(batch, unlocked[selectedChar] ? 1.0f : 0.5f);
    /*
        batch.draw(prev.getTexture(), prev.getX(), prev.getY(), prev.getOriginX(), prev.getOriginY(),
                prev.getWidth(), prev.getHeight(), prev.getScaleX(), prev.getScaleY(), prev.getRotation(),
                prev.getRegionX(), prev.getRegionY(), prev.getRegionWidth(), prev.getRegionHeight(), false, false);
        */
        prev.draw(batch, unlocked[previousItem()] ? 1.0f : 0.5f);
        /*batch.draw(next.getTexture(), next.getX(), next.getY(), next.getOriginX(), next.getOriginY(),
                next.getWidth(), next.getHeight(), next.getScaleX(), next.getScaleY(), next.getRotation(),
                next.getRegionX(), next.getRegionY(), next.getRegionWidth(), next.getRegionHeight(), false, false);
          */
        next.draw(batch, unlocked[nextItem()] ? 1.0f : 0.5f);
        if (player.MEMES >= prices[selectedChar] && !unlocked[selectedChar])
            smallerFont.setColor(Color.GOLD);
        else
            smallerFont.setColor(Color.WHITE);
        smallerFont.draw(batch, names[selectedChar], selected.getX() - names[selectedChar].length() / 2f, selected.getY() + selected.getHeight() * 1.4f);
        smallerFont.draw(batch, !unlocked[selectedChar] ? prices[selectedChar] + " Memes" : "Select", selected.getX() - names[selectedChar].length() / 2f,
                selected.getY() - 5.5f * names[selectedChar].length());

        smallFont.draw(batch, names[previousItem()], prev.getX() - names[previousItem()].length() / 2f, prev.getY() + prev.getHeight() * 1.4f);
        smallFont.draw(batch, names[nextItem()], next.getX() - names[nextItem()].length() / 2f, next.getY() + next.getHeight() * 1.4f);
        batch.end();
    }

    @Override
    public void update(float delta) {
        if (!MemeJump.gmsClient.isPaused()) {
            switch (state) {
                case TITLE:
                    cam.position.x = cam.viewportWidth / 2f;
                    cam.position.y = player.pos.y + 20;
                    cam.update();

                    sr.setProjectionMatrix(cam.combined);
                    batch.setProjectionMatrix(cam.combined);

                    if (!disconnected) {
                        stage.act(delta);

                        if (Gdx.input.justTouched() && !buttons[0].isPressed() && !buttons[1].isPressed()) {
                            if (multiMode) {
                                initMenuButtons();
                                multiMode = false;
                            } else {
                                if (singleMode) {
                                    started = true;
                                    Gdx.input.setInputProcessor(input);
                                }
                            }
                        }
                    } else {
                        if (Gdx.input.justTouched() && !error.isEmpty()) {
                            disconnected = false;
                            multiMode = false;
                            singleMode = true;
                            error = "";
                            initMenuButtons();
                        }
                    }
                    break;
                case PLAY:
                    player.update(delta, this);
                    if (!player.dead) {
                        for (int i = 0; i < tiles.length; i++) {
                            if (tiles[i].scrolledUp) {
                                if (i == 0)
                                    tiles[i].reset(tiles[tiles.length - 1]);
                                else
                                    tiles[i].reset(tiles[i - 1]);
                                player.SCORE++;
                            }
                            tiles[i].update(delta, this);
                        }


                        for (Meme meme : memes) meme.update(delta, this);

                        illuminati.update(delta, this);

                        cam.position.x = cam.viewportWidth / 2f;
                        cam.position.y = player.pos.y + 20;
                        cam.update();
                    } else {
//                        if (MemeJump.getGameResources().nicememe.isPlaying())
                        MemeJump.getGameResources().nicememe.stop();

                        shake.update(delta, cam);
                        cam.update();
                        if (shake.elapsed > shake.duration) {
                            MemeJump.prefs.putInteger("memes", player.MEMES);
                            MemeJump.prefs.flush();

                            MemeJump.prefs.putInteger("nwo", illuminati.kills);
                            MemeJump.prefs.flush();

                            if (player.SCORE > HIGHSCORE) {
                                MemeJump.prefs.putInteger("highscore", player.SCORE);
                                MemeJump.prefs.flush();
                                HIGHSCORE = player.SCORE;

                            }

                            deathSound.stop();


                            if (MemeJump.gmsClient.isConnected() && player.SCORE > 0)
                                MemeJump.gmsClient.submitScoreGPGS(player.SCORE);

                            initGameOverMenu();
                            Gdx.input.setInputProcessor(stage);

                            if (MemeJump.gmsClient.isConnected())
                                processAchievements();

                            /*if(Math.abs(cam.viewportWidth / 2f - cam.position.x) > 20f) {
                                cam.position.x += (cam.viewportWidth / 2f - cam.position.x) * delta * 1.5f;
                            }
                            else {
                                i
                            }*/
                            if (MathUtils.randomBoolean(0.55f) && MemeJump.gmsClient.isConnected())
                                MemeJump.gmsClient.showAd();
                            state = State.GAME_OVER;
                            nextClick = TimeUtils.millis();
                        } else {
                            illuminati.update(delta, this);
                        }
                    }
                    sr.setProjectionMatrix(cam.combined);
                    batch.setProjectionMatrix(cam.combined);
                    break;
                case GAME_OVER:
                    if (store) {
                        if (Gdx.input.isTouched()) {
                            Vector3 touch = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
                            cam.unproject(touch);
                            if (boardBounds.contains(touch.x, touch.y)) {
                                //Selection / Purchase
                                if (selected.getBoundingRectangle().contains(touch.x, touch.y)) {
                                    TweenCallback cb = new TweenCallback() {
                                        @Override
                                        public void onEvent(int type, BaseTween<?> source) {
                                            if (unlocked[selectedChar]) {
                                                MemeJump.prefs.putInteger("selectedChar", selectedChar);
                                                MemeJump.prefs.flush();
                                            }
                                        }
                                    };

                                    if (unlocked[selectedChar]) {
                                        MemeJump.getGameResources().wow.play();
                                        Tween.to(selected, SpriteAccessor.SCALE, 1.0f)
                                                .targetRelative(HEIGHT / 15f, HEIGHT / 15f)
                                                .delay(0.1f)
                                                .targetRelative(HEIGHT / 7.5f, HEIGHT / 7.5f)
                                                .setCallback(cb)        // callbacks can be specified to get notified of the completion
                                                .start(manager);
                                    } else {
                                        Tween.to(selected, SpriteAccessor.ALPHA, .8f).targetRelative(0.4f)
                                                .ease(TweenEquations.easeInOutQuad).targetRelative(1.0f).repeat(Tween.INFINITY, 0.3f)
                                                .setCallback(cb)
                                                .start(manager);
                                        MemeJump.getGameResources().error.play();
                                    }

                                    if (!unlocked[selectedChar] && player.MEMES >= prices[selectedChar]) {
                                        System.out.println("Purchased: " + names[selectedChar]);
                                        MemeJump.prefs.putBoolean(characters[selectedChar], true);
                                        player.MEMES -= prices[selectedChar];
                                        MemeJump.prefs.putInteger("memes", player.MEMES);
                                        MemeJump.prefs.putInteger("selectedChar", selectedChar);
                                        MemeJump.prefs.flush();

                                        if (names[selectedChar].equals("Harambe"))
                                            MemeJump.unlockAchievementGPGS("CgkI4-GW2_YVEAIQBw");

                                        if (names[selectedChar].equals("Doge"))
                                            MemeJump.unlockAchievementGPGS("CgkI4-GW2_YVEAIQBQ");

                                        if (names[selectedChar].equals("Dat Boi"))
                                            MemeJump.unlockAchievementGPGS("CgkI4-GW2_YVEAIQDA");

                                        manager.killTarget(selected, SpriteAccessor.ALPHA);
                                        MemeJump.getGameResources().cash.play();
                                        unlocked[selectedChar] = true;
                                    }

                                    if (!unlocked[selectedChar])
                                        MemeJump.INSTANCE.skin = "pepesad";
                                }
                            } else {
                                stage.clear();
                                initGameOverMenu();
                                store = false;
                            }
                        }
                        manager.update(delta);
                    }else{
                        if(disconnected && Gdx.input.justTouched()) {
                            disconnected = false;
                            error = "";
                            initGameOverMenu();
                        }
                    }
                    stage.act(delta);
                    break;
            }
        }

    }

    private int previousItem() {
        return selectedChar > 0 ? selectedChar - 1 : characters.length - 1;
    }

    private int nextItem() {
        return (selectedChar + 1) % characters.length;
    }

    private void processAchievements() {
        if (illuminati.kills >= 33)
            MemeJump.unlockAchievementGPGS("CgkI4-GW2_YVEAIQAg");

        if (player.SCORE >= 50)
            MemeJump.unlockAchievementGPGS("CgkI4-GW2_YVEAIQBA");

        if (player.MEMES >= 20)
            MemeJump.unlockAchievementGPGS("CgkI4-GW2_YVEAIQAQ");

        if (names[selectedChar].equals("PolandBall") && player.SCORE >= 35)
            MemeJump.unlockAchievementGPGS("CgkI4-GW2_YVEAIQAw ");
    }


    public void resetGame() {
        stage.clear();
        initMenuButtons();
        shake = new ShakingCam(15, 2);
        deathSound = MathUtils.randomBoolean() ? MemeJump.getGameResources().triggered : MemeJump.getGameResources().scream;
        player.reset(this);
        illuminati.reset(this);

        initSprites();

        cam.position.x = cam.viewportWidth / 2f;
        cam.position.y = player.pos.y + 20;
        cam.update();

        batch.setProjectionMatrix(cam.combined);
        sr.setProjectionMatrix(cam.combined);

        player.SCORE = 0;

        MemeJump.getGameResources().nicememe.play();
        MemeJump.getGameResources().nicememe.setLooping(true);

        Gdx.input.setInputProcessor(stage);
        this.state = State.TITLE;
    }

    /*public static void drawDashedLine(Color color, ShapeRenderer renderer, Vector2 dashes, Vector2 start, Vector2 end, float width) {
        if (dashes.x == 0) {
            return;
        }
        float dirX = end.x - start.x;
        float dirY = end.y - start.y;

        float length = Vector2.len(dirX, dirY);
        dirX /= length;
        dirY /= length;

        float curLen = 0;
        float curX = 0;
        float curY = 0;

        renderer.begin(ShapeRenderer.ShapeType.Filled);
        renderer.setColor(color);

        while (curLen <= length) {
            curX = (start.x + dirX * curLen);
            curY = (start.y + dirY * curLen);
            renderer.rectLine(curX, curY, curX + dirX * dashes.x, curY + dirY * dashes.x, width);
            curLen += (dashes.x + dashes.y);

        }
        renderer.end();
    }*/

    public void setError(String error) {
        this.error = error;
        disconnected = true;
    }

}
