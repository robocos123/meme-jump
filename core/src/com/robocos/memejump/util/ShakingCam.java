package com.robocos.memejump.util;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;

import java.util.Random;

/**
 * Created by Waleed on 8/19/2016.
 */
public class ShakingCam {

    public float intensity, duration, elapsed;

    private Random random;

    public ShakingCam(float intensity, float duration) {
        this.elapsed = 0;
        this.duration = duration;
        this.intensity = intensity;
        this.random = new Random();
    }

    public void update(float delta, OrthographicCamera camera) {
        if (elapsed < duration) {
            float currentPower = intensity * camera.zoom * ((duration - elapsed) / duration);
            float x = (random.nextFloat() - 0.5f) * 1.5f * currentPower / 2f;
            float y = (random.nextFloat() - 0.5f) * 1.5f * currentPower;
            camera.translate(-x, -y);

            elapsed += delta;
        }
    }
}