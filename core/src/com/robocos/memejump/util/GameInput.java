package com.robocos.memejump.util;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;
import com.robocos.memejump.MemeJump;
import com.robocos.memejump.screens.GameScreen;
import com.robocos.memejump.screens.MultiplayerScreen;
import com.robocos.memejump.screens.Screen;

/**
 * Created by Waleed on 8/16/2016.
 */
public class GameInput implements InputProcessor {

    private Screen screen;
    private MultiplayerScreen mscreen;

    public long nextTap;

    public GameInput(GameScreen screen) {
        this.screen = screen;
        nextTap = TimeUtils.millis();
    }

    public GameInput(MultiplayerScreen mscreen) {
        this.mscreen = mscreen;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(screen instanceof GameScreen) {
            GameScreen gscreen = (GameScreen) screen;
            switch (gscreen.state) {
                case TITLE:
                    if (gscreen.started) {
                        gscreen.state = GameScreen.State.PLAY;
                        if (TimeUtils.timeSinceMillis(nextTap) > 10)
                            movePlayer(gscreen, screenX, screenY);
                    }
                    break;
                case PLAY:
                    movePlayer(gscreen, screenX, screenY);
                    break;
            }
        }else if(screen instanceof MultiplayerScreen) {
            MultiplayerScreen mscreen = (MultiplayerScreen) screen;
            movePlayer(mscreen, screenX, screenY);
        }
        return true;
    }


    private void movePlayer(MultiplayerScreen screen, int screenX, int screenY) {
        System.out.println("Multimove");
        Vector3 touch = new Vector3(screenX, screenY, 0);
        screen.cam.unproject(touch);
        if (touch.x >= screen.WIDTH / 2f)
            screen.player.velocity = new Vector2(screen.player.width * 5f, screen.HEIGHT / 1.35f - screen.player.GRAVITY / 2.5f);
        else if (touch.x < screen.WIDTH / 2f)
            screen.player.velocity = new Vector2(-screen.player.width * 5f, screen.HEIGHT / 1.35f - screen.player.GRAVITY / 2.5f);
        screen.player.left = touch.x < screen.WIDTH / 2f;

        nextTap = TimeUtils.millis();

    }

    private void movePlayer(GameScreen screen, int screenX, int screenY) {
        Vector3 touch = new Vector3(screenX, screenY, 0);
        screen.cam.unproject(touch);
        if (touch.x >= screen.WIDTH / 2f)
            screen.player.velocity = new Vector2(screen.player.width * 5f, screen.HEIGHT / 1.35f - screen.player.GRAVITY / 2.5f);
        else if (touch.x < screen.WIDTH / 2f)
            screen.player.velocity = new Vector2(-screen.player.width * 5f, screen.HEIGHT / 1.35f - screen.player.GRAVITY / 2.5f);
        screen.player.left = touch.x < screen.WIDTH / 2f;

        nextTap = TimeUtils.millis();
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
