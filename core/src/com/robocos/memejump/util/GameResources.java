package com.robocos.memejump.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;


/**
 * Created by Waleed on 8/14/2016.
 */
public class GameResources {

    public Skin sprites, buttons, memes;
    public Texture TILE, ASTEROID, CLOUD;
    public Music nicememe;
    public Sound triggered, cash, scream, wow, victory_screech, error;

    public static BitmapFont bigFont, regularFont, smallerFont, smallFont;

    private static int HEIGHT = Gdx.graphics.getHeight();
    private static int WIDTH = Gdx.graphics.getWidth();

    public void load(OrthographicCamera cam) {


        sprites = new Skin(new TextureAtlas(internal("game/characters.pack")));
        memes = new Skin(new TextureAtlas(internal("game/memes.pack")));
        buttons = new Skin(new TextureAtlas(internal("gui/buttons.pack")));

        TILE = new Texture(internal("game/tile.png"));
        ASTEROID = new Texture(internal("game/asteroid.png"));
        CLOUD = new Texture(internal("game/cloud.png"));

        triggered = Gdx.audio.newSound(internal("sound/triggered.mp3"));
        nicememe = Gdx.audio.newMusic(internal("sound/song.mp3"));
        cash = Gdx.audio.newSound(internal("sound/buy.mp3"));
        scream = Gdx.audio.newSound(internal("sound/scream.mp3"));
        wow = Gdx.audio.newSound(internal("sound/wow.mp3"));
        victory_screech = Gdx.audio.newSound(internal("sound/victory_screech.mp3"));
        error = Gdx.audio.newSound(internal("sound/error.mp3"));

        bigFont = regenerateFonts(cam, (int) (HEIGHT / 7.68f));
        regularFont = regenerateFonts(cam, (int) (HEIGHT / 12.8f));
        smallerFont = regenerateFonts(cam, (int) (HEIGHT / 19.2f));
        smallFont = regenerateFonts(cam, (int) (HEIGHT / 35.4f));

        bigFont.setColor(new Color(65 / 255f, 104 / 255f, 37 / 255f, 0.7f));
        regularFont.setColor(new Color(65 / 255f, 104 / 255f, 37 / 255f, 1.0f));
        smallFont.setColor(new Color(65 / 255f, 104 / 255f, 37 / 255f, 1.0f));
    }

    public FileHandle internal(String loc) {
        return Gdx.files.internal(loc);
    }

    private BitmapFont regenerateFonts(OrthographicCamera cam, int size) {
        if (cam != null) {
            // recalculate sizes
            float ratioX = cam.viewportWidth / WIDTH;
            float ratioY = cam.viewportHeight / HEIGHT;
            System.out.println("Ratio: [" + ratioX + ":" + ratioY + "]");

            size *= ratioY;
        }

        // font parameters for this size
        FreeTypeFontGenerator.FreeTypeFontParameter params = new FreeTypeFontGenerator.FreeTypeFontParameter();
        params.characters = ".abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789:?!"; // your String containing all letters you need
        params.size = size;
        params.magFilter = Texture.TextureFilter.Linear; // used for resizing quality
        params.minFilter = Texture.TextureFilter.Linear; // also

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(internal("gui/atarian.ttf"));

        // make the font
        BitmapFont font = generator.generateFont(params);
        generator.dispose(); // dispose to avoid memory leaks

        return font;
    }
}
