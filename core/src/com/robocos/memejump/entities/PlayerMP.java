package com.robocos.memejump.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.robocos.memejump.MemeJump;
import com.robocos.memejump.screens.MultiplayerScreen;
import com.robocos.memejump.util.GameResources;

/**
 * Created by walee on 9/24/2016.
 */

public class PlayerMP extends Player {
    public float width, height;
    public Vector2 pos, netPos;
    public Vector2 velocity;
    public boolean dead, left;

    public int SCORE, MEMES;

    public String username, parID;
    public float delta;

    public PlayerMP(float x, float y, float width, float height, String skin, String username, String parID) {
        super(MemeJump.getGameResources().sprites.getRegion(skin));
        int HEIGHT = Gdx.graphics.getHeight();

        this.pos = new Vector2(x, y);
        this.netPos = new Vector2(x, y);
        this.velocity = new Vector2(width * 5f, HEIGHT / 1.35f - this.GRAVITY / 2.5f);

        this.width = width;
        this.height = height;

        this.username = username;
        this.parID = parID;
    }

    public void update(float delta) {
        this.pos = this.pos.lerp(netPos, delta * 5);
        if (dead)
            setRotation(180);
    }

    public void render(SpriteBatch batch) {
        batch.draw(this.getTexture(), pos.x, pos.y, getOriginX(), getOriginY(), width, height, getScaleX(), getScaleY(), getRotation(),
                getRegionX(), getRegionY(), getRegionWidth(), getRegionHeight(), left, false);
    }

    public void setStats(int score, int memes, boolean dead) {
        this.SCORE = score;
        this.MEMES = memes;
        this.dead = dead;
    }
}
