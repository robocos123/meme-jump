package com.robocos.memejump.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.robocos.memejump.MemeJump;
import com.robocos.memejump.screens.MultiplayerScreen;
import com.robocos.memejump.util.GameResources;
import com.robocos.memejump.screens.GameScreen;
import com.sun.org.apache.xpath.internal.axes.WalkingIterator;

/**
 * Created by Waleed on 8/14/2016.
 */
public class Player extends Sprite implements Comparable {

    public float width, height, GRAVITY = -MemeJump.HEIGHT / 31.57f;
    public Vector2 pos, velocity;
    public boolean dead, left;

    public String username;
    public int SCORE, MEMES;

    public int frames = 0;
    public String skin;

    public Player(float x, float y, float width, float height, String skin) {
        super(MemeJump.getGameResources().sprites.getRegion(skin));

        this.pos = new Vector2(x, y);
        this.velocity = new Vector2(0, 0);
        this.width = width;
        this.height = height;
        this.setOrigin(width / 2f, height / 2f);

        this.skin = skin;
    }

    public Player(float x, float y, float width, float height, String skin, String username) {
        this(x, y, width, height, skin);
        this.username = username;
    }

    public Player(TextureRegion region) {
        super(region);
    }


    public void update(float delta, GameScreen world) {
        if (!dead) {
            this.setBounds(this.pos.x, this.pos.y, width, height);
            if (this.pos.x > world.cam.viewportWidth - width || this.pos.x < 0 || collidesWith(world.illuminati) || this.pos.y < 0) {
                world.deathSound.play();
                dead = true;
                MemeJump.gmsClient.sendStatus(SCORE, MEMES, dead);
            }

            if (collidesWith(world.illuminati))
                world.illuminati.kills++;

            if (this.pos.y > 20)
                this.velocity.add(0, GRAVITY);
            this.pos.add(velocity.x * delta, velocity.y * delta);

            if (SCORE % 10 == 0 && SCORE > 0)
                GRAVITY += 0.5f;
        } else {
            this.setRotation(180);
            this.pos.y -= 2000f * delta;
        }
    }

    public void update(float timeStep, MultiplayerScreen world) {
        this.setBounds(this.pos.x, this.pos.y, width, height);
        if (!dead) {
            if (this.pos.x > world.cam.viewportWidth - width || this.pos.x < 0 || this.pos.y < 0) {
                world.deathSound.play();
                dead = true;
                MemeJump.gmsClient.sendStatus(SCORE, MEMES, dead);
            }

            if (this.pos.y > 20)
                this.velocity.add(0, GRAVITY);
            this.pos.add(velocity.x * timeStep, velocity.y * timeStep);

            if (SCORE % 10 == 0 && SCORE > 0)
                GRAVITY += 0.5f;
        } else {
            this.setRotation(180);
            this.pos.y -= 2000f * timeStep;
        }
        if(frames % 6 == 0)
            MemeJump.gmsClient.sendPos(new Vector2(pos.x / world.WIDTH, pos.y / world.HEIGHT), left);
    }

    public void reset(GameScreen world) {
        this.pos.set(world.WIDTH / 2, 20);
        this.velocity.set(0, 0);
        this.GRAVITY = -60.8f;
        this.setRotation(0);
        if (world.unlocked[world.selectedChar])
            this.setRegion(MemeJump.getGameResources().sprites.getRegion(world.characters[world.selectedChar]));
        else
            this.setRegion(MemeJump.getGameResources().sprites.getRegion("pepesad"));
        dead = false;
    }

    public void render(SpriteBatch batch) {
        batch.draw(this.getTexture(), pos.x, pos.y, getOriginX(), getOriginY(), width, height, getScaleX(), getScaleY(), getRotation(),
                getRegionX(), getRegionY(), getRegionWidth(), getRegionHeight(), left, false);
    }

    public boolean collidesWith(Illuminati il) {
        return Intersector.overlaps(this.getBoundingRectangle(), il.getBoundingRectangle()) && il.getY() > 0;
    }

    public String getUsername() {
        return username;
    }

    public String getSkin() {
        return skin;
    }

    @Override
    public int compareTo(Object o) {
        Player player = (Player) o;
        if(player != null && player.pos != null && this.pos != null) {
            if (this.SCORE - player.SCORE == 0)
                return (int) (this.pos.y - player.pos.y);
            return this.SCORE - player.SCORE;
        }
        return -1;
    }

    public void render(SpriteBatch batch, float x, float y) {
        rotate(100/60f);
        batch.draw(this.getTexture(), x, y, getOriginX(), getOriginY(), width, height, getScaleX(), getScaleY(), getRotation(),
                getRegionX(), getRegionY(), getRegionWidth(), getRegionHeight(), left, false);
    }
}
