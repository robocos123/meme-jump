package com.robocos.memejump.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.robocos.memejump.MemeJump;
import com.robocos.memejump.screens.MultiplayerScreen;
import com.robocos.memejump.util.GameResources;
import com.robocos.memejump.screens.GameScreen;

/**
 * Created by Waleed on 8/15/2016.
 */
public class Illuminati extends Sprite {

    public float x, y, width, height;
    public int kills;
    public float time;
    public float netY;

    public Illuminati(float x, float y, float width, float height) {
        super(MemeJump.getGameResources().sprites.getRegion("illuminati"));
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public void update(float delta, GameScreen world) {
        time += 2.5f / 60f;
        this.setBounds(this.x, this.y, width, height);
        float factor = !world.player.dead ? 1920 * (time * MathUtils.log2(time) + world.HEIGHT / 1.7f + world.player.GRAVITY / 2f) / world.HEIGHT : world.HEIGHT / (world.HEIGHT / 10f);
        y += factor * delta;
    }


    public void render(SpriteBatch batch) {
        batch.draw(this.getTexture(), x, y, getOriginX(), getOriginY(), width, height, getScaleX(), getScaleY(), getRotation(),
                getRegionX(), getRegionY(), getRegionWidth(), getRegionHeight(), false, false);
    }

    public void reset(GameScreen world) {
        this.x = world.WIDTH / 4;
        this.y = -1000;
        this.time = 0;
    }
}
