package com.robocos.memejump.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.robocos.memejump.MemeJump;
import com.robocos.memejump.util.GameResources;

/**
 * Created by walee on 9/16/2016.
 */
public class Cloud extends Sprite {
    public float x, y;

    public Cloud(float x, float y) {
        super(MemeJump.getGameResources().CLOUD);
        this.x = x;
        this.y = y;
    }

    public void update(float delta) {
    }

    public void render(SpriteBatch batch) {
//        batch.draw()
    }
}
