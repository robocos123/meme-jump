package com.robocos.memejump.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.robocos.memejump.MemeJump;
import com.robocos.memejump.screens.MultiplayerScreen;
import com.robocos.memejump.util.GameResources;
import com.robocos.memejump.screens.GameScreen;

import java.io.Serializable;

/**
 * Created by Waleed on 8/15/2016.
 */
public class Tile extends Sprite implements Serializable {
    public float x, y, width, height;
    public int id;
    public boolean scrolledUp = false, right, update = true;

    private GameScreen screen;

    public Tile(GameScreen screen, int id, float y, float width, float height, boolean right) {
        super(MemeJump.getGameResources().TILE);

        this.screen = screen;
        this.id = id;
        this.y = y;
        this.width = width;
        this.height = height;
        this.right = right;
        x = right ? screen.WIDTH - (screen.WIDTH / 2.45f) : screen.WIDTH / 70;

        this.setOrigin(width / 2f, height / 2f);
        this.setBounds(this.x, this.y, width, height);
    }

    public Tile(int id, float y, float width, float height, boolean right) {
        super(MemeJump.gameResources.TILE);
        int WIDTH = Gdx.graphics.getWidth();

        this.id = id;
        this.y = y;
        this.width = width;
        this.height = height;
        this.right = right;
        x = right ? WIDTH - (WIDTH / 2.45f) : WIDTH / 70;

        this.setOrigin(width / 2f, height / 2f);
        this.setBounds(this.x, this.y, width, height);
    }

    public void reset(Tile prevTile) {
        this.y = prevTile.y + (screen.HEIGHT / MathUtils.random(3.4f, 3.7f));
        id++;
        right = MathUtils.randomBoolean(prevTile.right ? 0.2f : 0.8f);
        x = right ? screen.WIDTH - (screen.WIDTH / 2.45f) : screen.WIDTH / 70;
        scrolledUp = false;
    }

    public void update(float delta, GameScreen screen) {
        this.setBounds(this.x, this.y, width, height);
        if (collidesWith(screen.player)) {
            screen.deathSound.play();
            screen.player.dead = true;
        }

        scrolledUp = screen.player.pos.y > this.y + 4 * screen.player.height;
    }

    public void update(float delta) {
        this.setBounds(this.x, this.y, width, height);
    }

    public void update(float delta, MultiplayerScreen screen) {

        this.setBounds(this.x, this.y, width, height);
        if (collidesWith(screen.player)) {
            screen.deathSound.play();
            screen.player.dead = true;
            MemeJump.gmsClient.sendStatus(screen.player.SCORE, screen.player.MEMES, screen.player.dead);
        }
        scrolledUp = screen.player.pos.y > this.y + 4 * screen.player.height;

    }

    private boolean collidesWith(Player player) {
        return Intersector.overlaps(player.getBoundingRectangle(), this.getBoundingRectangle());
    }

    private boolean collidesWith(PlayerMP player) {
        return Intersector.overlaps(player.getBoundingRectangle(), this.getBoundingRectangle());
    }

    public void render(SpriteBatch batch) {
        batch.draw(getTexture(), x, y, width, height);
    }
}
