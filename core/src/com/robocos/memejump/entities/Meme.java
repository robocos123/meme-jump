package com.robocos.memejump.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.robocos.memejump.MemeJump;
import com.robocos.memejump.screens.MultiplayerScreen;
import com.robocos.memejump.util.GameResources;
import com.robocos.memejump.screens.GameScreen;
import com.sun.org.apache.xpath.internal.operations.Mult;

import java.io.Serializable;

/**
 * Created by Waleed on 8/24/2016.
 */
public class Meme extends Sprite implements Serializable {

    public float x, y, width, height;
    public String skin;
    private GameScreen world;

    public int id;

    public boolean used;

    public Meme(GameScreen world, float y, float width, float height, String skin)  {
        super(MemeJump.getGameResources().memes.getRegion(skin));

        this.world = world;

        this.x = ((world.WIDTH - 400) / 2f - world.WIDTH / 16f) + MathUtils.random(300, 340);
        this.y = y;
        this.width = width;
        this.height = height;

        this.setOrigin(width / 2f, height / 2f);
        this.setBounds(x, y, width, height);
    }

    public Meme(int id, float x, float y, float width, float height, String skin) {
        super(MemeJump.getGameResources().memes.getRegion(skin));

        this.id = id;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.skin = skin;
    }

    public void reset() {
        this.x = ((world.WIDTH - 400) / 2 - world.WIDTH / 16) + MathUtils.random(300, 400);
        this.y += MathUtils.random(world.WIDTH * 4f, world.WIDTH * 8f);
        this.setRegion(MemeJump.getGameResources().memes.getRegion("meme" + MathUtils.random(1, 4)));
    }

    public void update(float delta, GameScreen world) {
        this.setBounds(x, y, width, height);
        if(Intersector.overlaps(world.player.getBoundingRectangle(), this.getBoundingRectangle()))
            world.player.MEMES++;

        if(world.player.pos.y > this.y + 4 * world.player.height
                || Intersector.overlaps(world.player.getBoundingRectangle(), this.getBoundingRectangle()))
            reset();
    }

    public void update(float delta, MultiplayerScreen world) {
        if(!used) {
            this.setBounds(x, y, width, height);
            if (Intersector.overlaps(world.player.getBoundingRectangle(), this.getBoundingRectangle())) {
                world.player.MEMES++;
                used = true;
                MemeJump.gmsClient.sendStatus(world.player.SCORE, world.player.MEMES, world.player.dead, id);
            }
        }
    }

    public void render(SpriteBatch batch) {
        batch.draw(this.getTexture(), x, y, getOriginX(), getOriginY(), width, height, getScaleX(), getScaleY(), getRotation(),
                getRegionX(), getRegionY(), getRegionWidth(), getRegionHeight(), false, false);
    }

}
